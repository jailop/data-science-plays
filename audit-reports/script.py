#!/usr/bin/python

import requests
from lxml import html

ADMINISTRATIVE_UNITS_FILE = "administrative_units.html"
BASE = "https://www.cortedecuentas.gob.sv"

def download_administrative_units(url):
    link = BASE + url
    page = requests.get(link, verify=False).content
    with open(ADMINISTRATIVE_UNITS_FILE, "wb") as fd:
        fd.write(page)

def extract_links_administrative_units():
    with open(ADMINISTRATIVE_UNITS_FILE, "rb") as fd:
        page = html.fromstring(fd.read())
        sections = page.xpath("//div[@class='pd-subcategory']")
        links = [link.xpath("a/@href")[0] for link in sections if link]
        print(links)
        return links
        
def extract_years(link):
    page = html.fromstring(requests.get(BASE + link, verify=False).content)
    years = page.xpath("//div[@class='pd-subcategory']/a")
    records = [
        {
            'year': year.xpath('text()')[0],
            'url': year.xpath('@href')[0]
        }
        for year in years
    ]
    return records
    
def extract_reports(filename, year, url):
    page = html.fromstring(requests.post(BASE + url, data={"limit": 0}, verify=False).content)
    sections = page.xpath("//div[@class='pd-filebox']")
    with open(filename, "a") as fd:
        for section in sections:
            title = section.xpath('.//a/text()')
            title = title[0] if len(title) > 0 else ''
            text = section.xpath(".//div[@class='pd-fdesc']/p/text()")
            text = text[0] if len(text) > 0 else ''
            link = section.xpath(".//a/@href")
            link = BASE + link[0] if len(link) > 0 else ''
            fd.write("{}|{}|{}|{}\n".format(
                year,
                title, 
                text,
                link,
            ))
    
def generate_report(filename, url):
    with open(filename, "w") as fd:
        fd.write("year|title|description|url\n")
    download_administrative_units(url)
    links = extract_links_administrative_units()
    for link in links:
        years = extract_years(link)
        for year in years:
            extract_reports(filename, year['year'], year['url'])

if __name__ == "__main__":
    audit_link = "/index.php/es/resultado-del-proceso-de-fiscalizacion/informes-finales-de-auditoria"
    generate_report("auditreports.csv", audit_link)
    trial_link = "/index.php/es/resultado-del-proceso-de-fiscalizacion/sentencias-definitivas-ejecutoriadas"
    generate_report("trialreports.csv", trial_link)
    
